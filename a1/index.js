/*
	Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
*/

function showSum(a,b){
	console.log(a + b);
};

showSum(6,3);

/*
	Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
*/

function showDifference(c,d){
	console.log(c - d);
};

showDifference(33,13);

/*
	Create function which will be able to multiply two numbers.
		-Numbers must be provided as arguments.
		-Return the result of the multiplication.

*/

function showProduct(e,f){
	return e*f;
};

/*
	-Create a new variable called product.
		-This product should be able to receive the result of multiplication function.
	Log the value of product variable in the console.
*/

let product = showProduct(5,9);
console.log(product);
